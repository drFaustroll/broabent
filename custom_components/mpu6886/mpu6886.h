#pragma once

#include "esphome/core/component.h"
#include "esphome/components/sensor/sensor.h"
#include "esphome/components/i2c/i2c.h"


#define MPU6886_ADDRESS           0x68
#define MPU6886_WHOAMI            0x75
#define MPU6886_ACCEL_INTEL_CTRL  0x69
#define MPU6886_SMPLRT_DIV        0x19
#define MPU6886_INT_PIN_CFG       0x37
#define MPU6886_INT_ENABLE        0x38
#define MPU6886_ACCEL_XOUT_H      0x3B
#define MPU6886_ACCEL_XOUT_L      0x3C
#define MPU6886_ACCEL_YOUT_H      0x3D
#define MPU6886_ACCEL_YOUT_L      0x3E
#define MPU6886_ACCEL_ZOUT_H      0x3F
#define MPU6886_ACCEL_ZOUT_L      0x40

#define MPU6886_TEMP_OUT_H        0x41
#define MPU6886_TEMP_OUT_L        0x42

#define MPU6886_GYRO_XOUT_H       0x43
#define MPU6886_GYRO_XOUT_L       0x44
#define MPU6886_GYRO_YOUT_H       0x45
#define MPU6886_GYRO_YOUT_L       0x46
#define MPU6886_GYRO_ZOUT_H       0x47
#define MPU6886_GYRO_ZOUT_L       0x48

#define MPU6886_USER_CTRL         0x6A
#define MPU6886_PWR_MGMT_1        0x6B
#define MPU6886_PWR_MGMT_2        0x6C
#define MPU6886_CONFIG            0x1A
#define MPU6886_GYRO_CONFIG       0x1B
#define MPU6886_ACCEL_CONFIG      0x1C
#define MPU6886_ACCEL_CONFIG2     0x1D
#define MPU6886_FIFO_EN           0x23

#define GRAVITY_EARTH 9.80665f
#define RtA     57.324841f
#define AtR     0.0174533f
#define Gyro_Gr 0.0010653f

namespace esphome {
  namespace mpu6886 {


    class MPU6886Component : public PollingComponent, public i2c::I2CDevice {
      public:

        enum accel_scale {
          AFS_2G = 0,
          AFS_4G,
          AFS_8G,
          AFS_16G
        };

        enum gyro_scale {
          GFS_250DPS = 0,
          GFS_500DPS,
          GFS_1000DPS,
          GFS_2000DPS
        };

        accel_scale acc_scale = AFS_16G;
        gyro_scale gyr_scale = GFS_2000DPS;

        void setup() override;
        void dump_config() override;

        void update() override;

        float get_setup_priority() const override;

        void set_accel_x_sensor(sensor::Sensor *accel_x_sensor) { accel_x_sensor_ = accel_x_sensor; }
        void set_accel_y_sensor(sensor::Sensor *accel_y_sensor) { accel_y_sensor_ = accel_y_sensor; }
        void set_accel_z_sensor(sensor::Sensor *accel_z_sensor) { accel_z_sensor_ = accel_z_sensor; }
        void set_temperature_sensor(sensor::Sensor *temperature_sensor) { temperature_sensor_ = temperature_sensor; }
        void set_gyro_x_sensor(sensor::Sensor *gyro_x_sensor) { gyro_x_sensor_ = gyro_x_sensor; }
        void set_gyro_y_sensor(sensor::Sensor *gyro_y_sensor) { gyro_y_sensor_ = gyro_y_sensor; }
        void set_gyro_z_sensor(sensor::Sensor *gyro_z_sensor) { gyro_z_sensor_ = gyro_z_sensor; }

        void set_accel_scale( accel_scale );
        void set_accel_res();

        void set_gyro_scale( gyro_scale );
        void set_gyro_res();


      protected:
        sensor::Sensor *accel_x_sensor_{nullptr};
        sensor::Sensor *accel_y_sensor_{nullptr};
        sensor::Sensor *accel_z_sensor_{nullptr};
        sensor::Sensor *temperature_sensor_{nullptr};
        sensor::Sensor *gyro_x_sensor_{nullptr};
        sensor::Sensor *gyro_y_sensor_{nullptr};
        sensor::Sensor *gyro_z_sensor_{nullptr};
        float accel_res {0.0f};
        float gyro_res {0.0f};
    };
    ;

  }  // namespace mpu6886
}  // namespace esphome
