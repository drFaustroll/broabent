#include "mpu6886.h"
#include "esphome/core/log.h"

namespace esphome {
  namespace mpu6886 {

    static const char *TAG = "mpu6886";

    void MPU6886Component::setup() {
      ESP_LOGCONFIG(TAG, "Setting up MPU6886...");
      uint8_t who_am_i;
      if (!this->read_byte(MPU6886_WHOAMI, &who_am_i)) {
        ESP_LOGD(TAG, "  Input who am I: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(who_am_i));
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "  Input who am I: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(who_am_i));
      if (who_am_i != 0x19 ){

        ESP_LOGD(TAG, "  wrong who am I expected 0x19, got: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(who_am_i));
        this->mark_failed();
        return;

      }
      delay(1);
      ESP_LOGD(TAG, "  Setting up Power Management...");
      // Setup power management
      uint8_t power_management {0x00};
      if (!this->read_byte(MPU6886_PWR_MGMT_1, &power_management)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input power_management: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(power_management));
      delay(10);
      power_management = 0x00;
      ESP_LOGD(TAG, "    Output power_management: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(power_management));
      if (!this->write_byte(MPU6886_PWR_MGMT_1, power_management)) {
        this->mark_failed();
        return;
      }
      delay(10);
      power_management = 0x80; //(0x01<<7)
      ESP_LOGD(TAG, "    Output power_management: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(power_management));
      if (!this->write_byte(MPU6886_PWR_MGMT_1, power_management)) {
        this->mark_failed();
        return;
      }
      delay(10);

      power_management = 0x01; //(0x01<<0)
      ESP_LOGD(TAG, "    Output power_management: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(power_management));
      if (!this->write_byte(MPU6886_PWR_MGMT_1, power_management)) {
        this->mark_failed();
        return;
      }
      delay(10);

      ESP_LOGD(TAG, "  Setting up Accel Config...");
      // Set range - 2G
      uint8_t accel_config;
      if (!this->read_byte(MPU6886_ACCEL_CONFIG, &accel_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input accel_config: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(accel_config));
      accel_config = (acc_scale << 3); //0x10;
      ESP_LOGD(TAG, "    Output accel_config: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(accel_config));
      if (!this->write_byte(MPU6886_ACCEL_CONFIG, accel_config)) {
        this->mark_failed();
        return;
      }
      delay(1);

      ESP_LOGD(TAG, "  Setting up Gyro Config...");
      // Set scale - 2000DPS
      uint8_t gyro_config;
      if (!this->read_byte(MPU6886_GYRO_CONFIG, &gyro_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input gyro_config: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(gyro_config));
      gyro_config = (gyr_scale << 3); // 0x18;
      ESP_LOGD(TAG, "    Output gyro_config: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(gyro_config));
      if (!this->write_byte(MPU6886_GYRO_CONFIG, gyro_config)) {
        this->mark_failed();
        return;
      }
      delay(1);

      ESP_LOGD(TAG, "  Setting up CONFIG...");
      uint8_t m_config;
      if (!this->read_byte(MPU6886_CONFIG, &m_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input CONFIG: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(m_config));
      m_config = 0x01;
      ESP_LOGD(TAG, "    Output CONFIG: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(m_config));
      if (!this->write_byte(MPU6886_CONFIG, m_config)) {
        this->mark_failed();
        return;
      }
      delay(1);

      ESP_LOGD(TAG, "  Setting up SMPLRT_DIV...");
      uint8_t smpl_config;
      if (!this->read_byte(MPU6886_SMPLRT_DIV, &smpl_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input SMPLRT_DIV: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(smpl_config));
      smpl_config = 0x05;
      ESP_LOGD(TAG, "    Output SMPLRT_DIV: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(smpl_config));
      if (!this->write_byte(MPU6886_SMPLRT_DIV, smpl_config)) {
        this->mark_failed();
        return;
      }
      delay(1);

      ESP_LOGD(TAG, "  Setting up INT_ENABLE...");
      uint8_t int_config;
      if (!this->read_byte(MPU6886_INT_ENABLE, &int_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input INT_ENABLE: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(int_config));
      int_config = 0x00;
      ESP_LOGD(TAG, "    Output INT_ENABLE: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(int_config));
      if (!this->write_byte(MPU6886_INT_ENABLE, int_config)) {
        this->mark_failed();
        return;
      }
      delay(1);


      ESP_LOGD(TAG, "  Setting up ACCEL_CONFIG_2...");
      uint8_t a_config;
      if (!this->read_byte(MPU6886_ACCEL_CONFIG2, &a_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input ACCEL_CONFIG_2: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(a_config));
      a_config = 0x00;
      ESP_LOGD(TAG, "    Output ACCEL_CONFIG_2: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(a_config));
      if (!this->write_byte(MPU6886_ACCEL_CONFIG2, a_config)) {
        this->mark_failed();
        return;
      }
      delay(1);

      ESP_LOGD(TAG, "  Setting up USER_CTRL...");
      uint8_t u_config;
      if (!this->read_byte(MPU6886_USER_CTRL, &u_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input USER_CTRL: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(u_config));
      u_config = 0x00;
      ESP_LOGD(TAG, "    Output USER_CTRL: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(u_config));
      if (!this->write_byte(MPU6886_USER_CTRL, u_config)) {
        this->mark_failed();
        return;
      }
      delay(1);

      ESP_LOGD(TAG, "  Setting up FIFO_EN...");
      uint8_t f_config;
      if (!this->read_byte(MPU6886_FIFO_EN, &f_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input FIFO_EN: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(f_config));
      f_config = 0x00;
      ESP_LOGD(TAG, "    Output FIFO_EN: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(f_config));
      if (!this->write_byte(MPU6886_FIFO_EN, f_config)) {
        this->mark_failed();
        return;
      }
      delay(1);

      ESP_LOGD(TAG, "  Setting up INT_PIN_CFG...");
      uint8_t ii_config;
      if (!this->read_byte(MPU6886_INT_PIN_CFG, &ii_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input INT_PIN_CFG: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(ii_config));
      ii_config = 0x22;
      ESP_LOGD(TAG, "    Output INT_PIN_CFG: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(ii_config));
      if (!this->write_byte(MPU6886_INT_PIN_CFG, ii_config)) {
        this->mark_failed();
        return;
      }
      delay(1);

      ESP_LOGD(TAG, "  Setting up INT_ENABLE...");
      if (!this->read_byte(MPU6886_INT_ENABLE, &int_config)) {
        this->mark_failed();
        return;
      }
      ESP_LOGD(TAG, "    Input int_enable: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(int_config));
      int_config = 0x01;
      ESP_LOGD(TAG, "    Output int_enable: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(int_config));
      if (!this->write_byte(MPU6886_INT_PIN_CFG, int_config)) {
        this->mark_failed();
        return;
      }
      delay(100);

      set_accel_scale(acc_scale);
      delay(1);
      set_gyro_scale(gyr_scale);
      delay(1);

    }

    void MPU6886Component::dump_config() {
      ESP_LOGCONFIG(TAG, "MPU6886:");
      LOG_I2C_DEVICE(this);
      if (this->is_failed()) {
        ESP_LOGE(TAG, "Communication with MPU6886 failed!");
      }
      LOG_UPDATE_INTERVAL(this);
      LOG_SENSOR("  ", "Acceleration X", this->accel_x_sensor_);
      LOG_SENSOR("  ", "Acceleration Y", this->accel_y_sensor_);
      LOG_SENSOR("  ", "Acceleration Z", this->accel_z_sensor_);
      LOG_SENSOR("  ", "Gyro X", this->gyro_x_sensor_);
      LOG_SENSOR("  ", "Gyro Y", this->gyro_y_sensor_);
      LOG_SENSOR("  ", "Gyro Z", this->gyro_z_sensor_);
      LOG_SENSOR("  ", "Temperature", this->temperature_sensor_);
    }

    void MPU6886Component::update() {
      ESP_LOGV(TAG, "    Updating MPU6886...");

      uint8_t raw_data[14];

      if (!this->read_bytes(MPU6886_ACCEL_XOUT_H, &raw_data[0], 6)) {
        this->status_set_warning();
        return;
      }
      if (!this->read_bytes(MPU6886_GYRO_XOUT_H, &raw_data[6], 6)) {
        this->status_set_warning();
        return;
      }
      if (!this->read_bytes(MPU6886_TEMP_OUT_H, &raw_data[12], 2)) {
        this->status_set_warning();
        return;
      }
      float accel_x = (static_cast<int16_t>((raw_data[0]<<8)) | raw_data[1]) * accel_res;
      float accel_y = (static_cast<int16_t>((raw_data[2]<<8)) | raw_data[3]) * accel_res;
      float accel_z = (static_cast<int16_t>((raw_data[4]<<8)) | raw_data[5]) * accel_res;

      auto *data = reinterpret_cast<int16_t *>(raw_data);

      float temperature = ((raw_data[12]<<8)|raw_data[13]) / 326.8f + 25.0f;

      float gyro_x = (static_cast<int16_t>((raw_data[6]<<8)) | raw_data[7]) * gyro_res;
      float gyro_y = (static_cast<int16_t>((raw_data[8]<<8)) | raw_data[9]) * gyro_res;
      float gyro_z = (static_cast<int16_t>((raw_data[10]<<8)) | raw_data[11]) * gyro_res;

      ESP_LOGD(TAG,
          "Got accel={x=%.3f m/s², y=%.3f m/s², z=%.3f m/s²}, "
          "gyro={x=%.3f °/s, y=%.3f °/s, z=%.3f °/s}, temp=%.3f°C",
          accel_x, accel_y, accel_z, gyro_x, gyro_y, gyro_z, temperature);

      if (this->accel_x_sensor_ != nullptr)
        this->accel_x_sensor_->publish_state(accel_x);
      if (this->accel_y_sensor_ != nullptr)
        this->accel_y_sensor_->publish_state(accel_y);
      if (this->accel_z_sensor_ != nullptr)
        this->accel_z_sensor_->publish_state(accel_z);

      if (this->temperature_sensor_ != nullptr)
        this->temperature_sensor_->publish_state(temperature);

      if (this->gyro_x_sensor_ != nullptr)
        this->gyro_x_sensor_->publish_state(gyro_x);
      if (this->gyro_y_sensor_ != nullptr)
        this->gyro_y_sensor_->publish_state(gyro_y);
      if (this->gyro_z_sensor_ != nullptr)
        this->gyro_z_sensor_->publish_state(gyro_z);

      this->status_clear_warning();
    }
    float MPU6886Component::get_setup_priority() const { return setup_priority::DATA; }

    void MPU6886Component::set_accel_scale(accel_scale scale) {

      uint8_t as_config;
      ESP_LOGD(TAG, "  Change accel scale");
      if (!this->read_byte(MPU6886_ACCEL_CONFIG, &as_config)) {
        this->mark_failed();
        return;
      }
      delay(1.0);
      ESP_LOGD(TAG, "    Input accel_config: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(as_config));

      as_config = (scale<<3);
      ESP_LOGD(TAG, "    Output change accel_config to: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(as_config));

      if (!this->write_byte(MPU6886_ACCEL_CONFIG, as_config)) {
        this->mark_failed();
        return;
      }

      acc_scale = scale;
      set_accel_res();

    }

    void MPU6886Component::set_gyro_scale(gyro_scale scale) {
      uint8_t gr_config;
      ESP_LOGD(TAG, "  Change gyro scale");
      if (!this->read_byte(MPU6886_GYRO_CONFIG, &gr_config)) {
        this->mark_failed();
        return;
      }
      delay(1.0);
      ESP_LOGD(TAG, "    Input gyro_config: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(gr_config));

      gr_config = (scale<<3);
      ESP_LOGD(TAG, "    Output change gyro_config to: 0b" BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(gr_config));

      if (!this->write_byte(MPU6886_GYRO_CONFIG, gr_config)) {
        this->mark_failed();
        return;
      }

      gyr_scale = scale;
      set_gyro_res();
    }

    void MPU6886Component::set_accel_res() {
      switch (acc_scale)
      {
        // Possible accelerometer scales (and their register bit settings) are:
        // 2 Gs (00), 4 Gs (01), 8 Gs (10), and 16 Gs  (11).
        // Here's a bit of an algorithm to calculate DPS/(ADC tick) based on that 2-bit value:
        case AFS_2G:
          accel_res = 2.0/32768.0;
          break;
        case AFS_4G:
          accel_res = 4.0/32768.0;
          break;
        case AFS_8G:
          accel_res = 8.0/32768.0;
          break;
        case AFS_16G:
          accel_res = 16.0/32768.0;
          break;
      }
    }

    void MPU6886Component::set_gyro_res() {

      switch (gyr_scale)
      {
        // Possible gyro scales (and their register bit settings) are:
        case GFS_250DPS:
          gyro_res = 250.0/32768.0;
          break;
        case GFS_500DPS:
          gyro_res = 500.0/32768.0;
          break;
        case GFS_1000DPS:
          gyro_res = 1000.0/32768.0;
          break;
        case GFS_2000DPS:
          gyro_res = 2000.0/32768.0;
          break;
      }
    }

  }  // namespace mpu6886
}  // namespace esprrhome
